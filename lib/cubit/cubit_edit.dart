import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:notes_app/model/model_edit.dart';
import '../responsitory/repository_edit.dart';
import '../state/state_edit.dart';

class EditProfileCubit extends Cubit<EditProfileState> {//init EditProfile Cubit
  final EditProfileRepository _repository;

  EditProfileCubit(this._repository) : super(InitialEditProfileState());

  Future<void> updateProfile(UpdateProfile updateProfile) async {//! return body from API
    try {
      var result = await _repository.updateProfile(updateProfile);//result to save body
      emit(SuccessEditProfileState(result));
    } catch (e) {
      emit(FailureEditProfileState(e.toString()));
    }
  }
}