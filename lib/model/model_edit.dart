class EditProfile {
  final int? status;

  const EditProfile({required this.status});//constructor

  factory EditProfile.fromJson(Map<String, dynamic> json) {
    return EditProfile(status: json['status']);
  }
}

class UpdateProfile {
  String? emailCurrent;
  String? emailNew;
  String? firstName;
  String? lastName;

  UpdateProfile(this.emailCurrent, this.emailNew, this.firstName,this.lastName);
}